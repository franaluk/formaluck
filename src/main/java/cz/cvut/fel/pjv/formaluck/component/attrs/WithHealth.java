package cz.cvut.fel.pjv.formaluck.component.attrs;

/**
 * WithHealth
 */
public interface WithHealth {

    public int getHealth();

    public void setHealth(int health);
}