package cz.cvut.fel.pjv.formaluck;

/**
 * Constants
 * 
 * Constants to use in engine.
 */
public class Constants {

    public static final String DIRECTION_UP = "UP";
    public static final String DIRECTION_DOWN = "DOWN";
    public static final String DIRECTION_LEFT = "LEFT";
    public static final String DIRECTION_RIGHT = "RIGHT";

    public static final int MOVE = 5;
    public static final int SIZE = 40;
    public static final int FPS = 60;

    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;
    public static final int HUD_HEIGHT = 200;
    public static final int AREA_WIDTH = Constants.WIDTH;
    public static final int AREA_HEIGHT = Constants.HEIGHT - Constants.HUD_HEIGHT;
    
    public static final String ICONS_DIR = "/icons/";
}
