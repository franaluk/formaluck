package cz.cvut.fel.pjv.formaluck.component;

import cz.cvut.fel.pjv.formaluck.Constants;
import javafx.geometry.Rectangle2D;

/**
 * Position
 * 
 * Position on game area.
 */
public class Position {
    public int x;
    public int y;
    public int w;
    public int h;
    Component component;

    public Position(int x, int y) {
        this(x, y, Constants.SIZE, Constants.SIZE);
    }

    public Position(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public void move(int speed, String direction) {
        switch (direction) {
        case Constants.DIRECTION_UP:
            moveUp(speed);
            break;
        case Constants.DIRECTION_DOWN:
            moveDown(speed);
            break;
        case Constants.DIRECTION_LEFT:
            moveLeft(speed);
            break;
        case Constants.DIRECTION_RIGHT:
            moveRight(speed);
            break;
        default:
            break;
        }
    }

    public void moveUp(int speed) {
        y -= speed;
    }

    public void moveDown(int speed) {
        y += speed;
    }

    public void moveLeft(int speed) {
        x -= speed;
    }

    public void moveRight(int speed) {
        x += speed;
    }

    public Rectangle2D getBounds() {
        return new Rectangle2D(x, y, w, h);
    }
}