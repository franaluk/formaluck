package cz.cvut.fel.pjv.formaluck.component;

import java.util.Iterator;

import cz.cvut.fel.pjv.formaluck.Constants;
import cz.cvut.fel.pjv.formaluck.component.attrs.WithHealth;
import cz.cvut.fel.pjv.formaluck.component.attrs.WithWeapon;
import cz.cvut.fel.pjv.formaluck.engine.Event;
import cz.cvut.fel.pjv.formaluck.engine.Lambda;
import cz.cvut.fel.pjv.formaluck.inventory.Bow;
import cz.cvut.fel.pjv.formaluck.inventory.InventoryItem;
import cz.cvut.fel.pjv.formaluck.inventory.Weapon;
import javafx.scene.input.KeyCode;

/**
 * Player
 * 
 * Playable component controlled by user.
 */
public class Player extends Component implements WithHealth, WithWeapon {
    boolean isFighting = false;
    String currentDirection = "";
    String lastDirection = Constants.DIRECTION_RIGHT;
    Weapon Weapon = null;
    InventoryItem inventoryItem = null;
    int health;
    int maxHealth = 100;
    int level = 1;
    int attackSpeed = 200;

    public Player(Position position) {
        super(position);
        this.health = this.maxHealth;
    }

    @Override
    public Event tick() {
        if (currentDirection != "")
            return new Event((Lambda) (app) -> {
                move(currentDirection);
            });

        return null;
    }

    public Event keyReleased(KeyCode code) {
        if (!isFighting)
            this.currentDirection = "";

        return null;
    }

    @Override
    public Weapon getWeapon() {
        return Weapon;
    }

    @Override
    public void setWeapon(Weapon Weapon) {
        this.Weapon = Weapon;
    }

    /**
     * @return the lastDirection
     */
    public String getLastDirection() {
        return lastDirection;
    }

    /**
     * @return the attackSpeed
     */
    public int getAttackSpeed() {
        return attackSpeed;
    }

    @Override
    public int getAttackTime() {
        return attackSpeed;
    }

    /**
     * @return the health
     */
    public int getHealth() {
        return health;
    }

    /**
     * @param health the health to set
     */
    public void setHealth(int health) {
        this.health = health > maxHealth ? maxHealth : health;
    }

    /**
     * @return the maxHealth
     */
    public int getMaxHealth() {
        return maxHealth;
    }

    /**
     * @return the inventoryItem
     */
    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public Event keyPressed(KeyCode code) {
        if (code == KeyCode.UP) {
            this.currentDirection = this.lastDirection = Constants.DIRECTION_UP;
        } else if (code == KeyCode.DOWN) {
            this.currentDirection = this.lastDirection = Constants.DIRECTION_DOWN;
        } else if (code == KeyCode.LEFT) {
            this.currentDirection = this.lastDirection = Constants.DIRECTION_LEFT;
        } else if (code == KeyCode.RIGHT) {
            this.currentDirection = this.lastDirection = Constants.DIRECTION_RIGHT;
        } else if (code == KeyCode.SPACE && hasWeapon()) {
            return Weapon.use(this);
        } else if (code == KeyCode.E) {
            return changeWeapon();
        } else if (code == KeyCode.Q) {
            return changeInventoryItem();
        } else if (code == KeyCode.ENTER && inventoryItem != null) {
            try {
                return inventoryItem.use();
            } catch (Exception e) {
                return null;
            }
        } else if (code == KeyCode.DELETE && inventoryItem != null) {
            return new Event(app -> {
                app.inventory.items.remove(inventoryItem);

                if (inventoryItem instanceof Weapon)
                    changeWeapon().trigger(app);

                changeInventoryItem().trigger(app);
            });
        }

        return null;
    }

    public Event interactWith(Component component) {
        if (component instanceof Wall) {
            switch (currentDirection) {
            case Constants.DIRECTION_UP:
                move(Constants.DIRECTION_DOWN);
                break;
            case Constants.DIRECTION_DOWN:
                move(Constants.DIRECTION_UP);
                break;
            case Constants.DIRECTION_LEFT:
                move(Constants.DIRECTION_RIGHT);
                break;
            case Constants.DIRECTION_RIGHT:
                move(Constants.DIRECTION_LEFT);
                break;
            default:
                break;
            }
            currentDirection = "";

        } else if (component instanceof InventoryItem) {
            if (component instanceof Weapon && !hasWeapon())
                Weapon = (Weapon) component;

            if (inventoryItem == null)
                inventoryItem = (InventoryItem) component;

            return new Event(app -> {
                app.inventory.items.add((InventoryItem) component);
                app.components.remove((Component) component);
            });
        }

        return null;
    }

    public boolean hasWeapon() {
        return Weapon != null;
    }

    public Event changeInventoryItem() {
        return new Event(app -> {
            InventoryItem firstInventoryItem = null;
            boolean next = false;

            for (InventoryItem item : app.inventory.items) {
                if (firstInventoryItem == null)
                    firstInventoryItem = item;

                if (next) {
                    inventoryItem = item;
                    return;
                }

                if (item == inventoryItem)
                    next = true;
            }

            inventoryItem = firstInventoryItem;
        });
    }

    private Event changeWeapon() {
        return new Event(app -> {
            Iterator<InventoryItem> i = app.inventory.items.iterator();
            Weapon firstWeapon = null;
            boolean next = false;

            while (i.hasNext()) {
                InventoryItem item = i.next();

                if (item instanceof Weapon) {
                    if (firstWeapon == null)
                        firstWeapon = (Weapon) item;

                    if (next) {
                        Weapon = (Weapon) item;
                        return;
                    }

                    if (item == Weapon)
                        next = true;
                }
            }

            Weapon = firstWeapon;
        });
    }

    public void move(String direction) {
        switch (direction) {
        case Constants.DIRECTION_UP:
            position.moveUp(Constants.MOVE);
            break;

        case Constants.DIRECTION_DOWN:
            position.moveDown(Constants.MOVE);
            break;

        case Constants.DIRECTION_LEFT:
            position.moveLeft(Constants.MOVE);
            break;

        case Constants.DIRECTION_RIGHT:
            position.moveRight(Constants.MOVE);
            break;

        default:
            break;
        }
    }
}