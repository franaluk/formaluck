package cz.cvut.fel.pjv.formaluck.engine;

import cz.cvut.fel.pjv.formaluck.App;

/**
 * Lambda
 * 
 * Used for anonymous functions with app context.
 */
@FunctionalInterface
public interface Lambda {
    void run(App app);
}
