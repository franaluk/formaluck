package cz.cvut.fel.pjv.formaluck.inventory;

import java.util.ArrayList;

/**
 * Inventory
 * 
 * User inventory. Collection of items to use.
 */
public class Inventory {
    public ArrayList<InventoryItem> items;

    public Inventory() {
        items = new ArrayList<InventoryItem>();
    }
}