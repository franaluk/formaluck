package cz.cvut.fel.pjv.formaluck.inventory;

import cz.cvut.fel.pjv.formaluck.component.Component;
import cz.cvut.fel.pjv.formaluck.component.Position;
import cz.cvut.fel.pjv.formaluck.engine.Event;

/**
 * InventoryItem
 * 
 * Inventory item to use, eg. Potion, Weapon...
 */
public abstract class InventoryItem extends Component {
    protected Component owner;

    public InventoryItem(Position position) {
        super(position);
    }

    public abstract Event use() throws Exception;

    /**
     * @return the owner
     */
    public Component getOwner() {
        return owner;
    }
}