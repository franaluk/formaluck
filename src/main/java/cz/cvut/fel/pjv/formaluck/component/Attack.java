package cz.cvut.fel.pjv.formaluck.component;

import cz.cvut.fel.pjv.formaluck.component.attrs.WithHealth;
import cz.cvut.fel.pjv.formaluck.component.attrs.WithWeapon;
import cz.cvut.fel.pjv.formaluck.engine.Event;
import cz.cvut.fel.pjv.formaluck.inventory.Weapon;

/**
 * Attack
 */
public class Attack extends Component implements WithWeapon {
    protected Weapon weapon;
    protected boolean isUsed = false;

    public Attack(Position position, Weapon weapon) {
        super(position);
        this.weapon = weapon;
    }

    @Override
    public Weapon getWeapon() {
        return weapon;
    }

    @Override
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public int getAttackSpeed() {
        return 0;
    }

    @Override
    public int getAttackTime() {
        return 0;
    }

    @Override
    public Event interactWith(Component component) {
        if (!isUsed && component != weapon.getOwner() && component instanceof WithHealth) {
            return new Event(app -> {
                isUsed = true;

                WithHealth withHealthComponent = (WithHealth) component;
                withHealthComponent.setHealth(withHealthComponent.getHealth() - weapon.getDamage());
            });
        }

        return null;
    }
}