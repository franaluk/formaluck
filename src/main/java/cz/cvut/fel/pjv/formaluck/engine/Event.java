package cz.cvut.fel.pjv.formaluck.engine;

import cz.cvut.fel.pjv.formaluck.App;

/**
 * Event
 * 
 * Game event. Use for any game action.
 */
public class Event {
    Lambda lambda;
    // ArrayList<Event> list;

    public Event(Lambda lambda) {
        this.lambda = lambda;
        // this.list = list;
    }

    public void trigger(App app) {
        lambda.run(app);
        // app.events.remove(this);
        // list.remove(this);
    }
}
