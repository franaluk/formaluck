package cz.cvut.fel.pjv.formaluck.engine;

import cz.cvut.fel.pjv.formaluck.App;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

/**
 * ActionHandler
 * 
 * Handles user input and actions, eg. mouse and keyboard events.
 */
public class ActionHandler implements EventHandler<KeyEvent> {
    App app;

    public ActionHandler(App app) {
        this.app = app;
    }

    @Override
    public void handle(KeyEvent event) {
        if (event.getEventType() == KeyEvent.KEY_PRESSED) {
            app.events.add(app.player.keyPressed(event.getCode()));
        } else if (event.getEventType() == KeyEvent.KEY_RELEASED) {
            app.events.add(app.player.keyReleased(event.getCode()));
        }
    }
}
