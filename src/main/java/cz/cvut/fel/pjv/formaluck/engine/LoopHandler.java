package cz.cvut.fel.pjv.formaluck.engine;

import javafx.event.EventHandler;
import javafx.event.ActionEvent;

/**
 * LoopHandler
 * 
 * Handles loop in game.
 */
public class LoopHandler implements EventHandler<ActionEvent> {

    private final GameArea gameArea;

    public LoopHandler(GameArea gameArea) {
        this.gameArea = gameArea;
        this.gameArea.redraw();
    }

    @Override
    public void handle(ActionEvent event) {
        this.gameArea.redraw();
    }
}
