package cz.cvut.fel.pjv.formaluck.component;

import cz.cvut.fel.pjv.formaluck.Constants;
import cz.cvut.fel.pjv.formaluck.component.attrs.WithHealth;
import cz.cvut.fel.pjv.formaluck.component.attrs.WithWeapon;
import cz.cvut.fel.pjv.formaluck.engine.Event;
import cz.cvut.fel.pjv.formaluck.inventory.Weapon;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * Arrow
 */
public class Arrow extends Attack {
    String direction;

    public Arrow(Position position, Weapon weapon, String direction) {
        super(position, weapon);
        this.direction = direction;
        this.image = new Image(Constants.ICONS_DIR + getComputedImage() + ".png");
    }

    @Override
    public Event tick() {
        return new Event(app -> {
            position.move(10, direction);
        });
    }

    @Override
    public Event interactWith(Component component) {
        if (weapon.getOwner() != component) {
            return new Event(app -> {
                app.components.remove(this);

                if (component instanceof WithHealth) {
                    WithHealth withHealthComponent = (WithHealth) component;
                    withHealthComponent.setHealth(withHealthComponent.getHealth() - weapon.getDamage());
                }
            });
        }

        return null;
    }

    public String getComputedImage() {
        switch (direction) {
        case Constants.DIRECTION_UP:
            return "arrow-up";
        case Constants.DIRECTION_DOWN:
            return "arrow-down";
        case Constants.DIRECTION_LEFT:
            return "arrow-left";
        case Constants.DIRECTION_RIGHT:
            return "arrow-right";
        default:
            return "bow";
        }
    }
}