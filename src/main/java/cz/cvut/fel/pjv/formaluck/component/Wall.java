package cz.cvut.fel.pjv.formaluck.component;

/**
 * Wall
 * 
 * Static wall. Cannot be stepped on.
 */
public class Wall extends Component {
    Position position;

    public Wall(Position position) {
        super(position);
        this.position = position;
    }
}