package cz.cvut.fel.pjv.formaluck;

import java.util.ArrayList;
import java.util.Arrays;

import cz.cvut.fel.pjv.formaluck.component.Chest;
import cz.cvut.fel.pjv.formaluck.component.Component;
import cz.cvut.fel.pjv.formaluck.component.Enemy;
import cz.cvut.fel.pjv.formaluck.component.Player;
import cz.cvut.fel.pjv.formaluck.component.Position;
import cz.cvut.fel.pjv.formaluck.component.Wall;
import cz.cvut.fel.pjv.formaluck.engine.ActionHandler;
import cz.cvut.fel.pjv.formaluck.engine.Event;
import cz.cvut.fel.pjv.formaluck.engine.GameArea;
import cz.cvut.fel.pjv.formaluck.engine.LoopHandler;
import cz.cvut.fel.pjv.formaluck.inventory.Bow;
import cz.cvut.fel.pjv.formaluck.inventory.Inventory;
import cz.cvut.fel.pjv.formaluck.inventory.InventoryItem;
import cz.cvut.fel.pjv.formaluck.inventory.Potion;
import cz.cvut.fel.pjv.formaluck.inventory.Weapon;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.TimelineBuilder;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * App
 * 
 * Main application.
 */
@SuppressWarnings("deprecation")
public class App extends Application {
    public GameArea gameArea;
    public LoopHandler loopHandler;
    public ActionHandler actionHandler;
    public ArrayList<Component> components;
    public ArrayList<Component> newComponents;
    public ArrayList<Event> events;
    public Player player;
    public Inventory inventory;

    @Override
    public void init() throws Exception {
        this.player = new Player(new Position(300, 300, (int) (Constants.SIZE * 1.5), (int) (Constants.SIZE * 1.5)));
        this.inventory = new Inventory();
        this.events = new ArrayList<Event>();
        this.components = new ArrayList<Component>();
        this.newComponents = new ArrayList<Component>();
        this.components.add(player);
        this.actionHandler = new ActionHandler(this);
        this.gameArea = new GameArea(this);
        this.loopHandler = new LoopHandler(gameArea);

        this.loadLevel();
    }

    private void loadLevel() {
        for (int i = 0; i < Constants.AREA_WIDTH / Constants.SIZE; i++) {
            Wall w = new Wall(new Position(i * Constants.SIZE, 0, Constants.SIZE, Constants.SIZE));
            components.add(w);
            w = new Wall(new Position(i * Constants.SIZE, Constants.AREA_HEIGHT - Constants.SIZE, Constants.SIZE,
                    Constants.SIZE));
            components.add(w);
        }

        for (int j = 0; j < Constants.AREA_HEIGHT / Constants.SIZE; j++) {
            Wall w = new Wall(new Position(0, j * Constants.SIZE, Constants.SIZE, Constants.SIZE));
            components.add(w);
            w = new Wall(new Position(Constants.AREA_WIDTH - Constants.SIZE, j * Constants.SIZE, Constants.SIZE,
                    Constants.SIZE));
            components.add(w);
        }

        components.add(new Enemy(new Position(352, 405)));
        components.add(new Enemy(new Position(812, 356)));
        components.add(new Enemy(new Position(947, 56)));
        components.add(new Potion(new Position(498, 213), 15));
        components.add(new Potion(new Position(423, 159), 20));
        components.add(new Potion(new Position(310, 352), -50));
        components.add(new Potion(new Position(567, 38), -20));
        components.add(new Potion(new Position(605, 208), 0));
        components.add(new Bow(new Position(200, 200), 10));
        components.add(new Weapon(new Position(213, 264), 10));
        components.add(new Weapon(new Position(247, 298), 10));
        components.add(new Chest(new Position(250, 200),
                new ArrayList<InventoryItem>(Arrays.asList(new Weapon(null, 10), new Potion(null, 30), new Potion(null, 30),
                        new Potion(null, 30), new Potion(null, 30), new Potion(null, 30), new Potion(null, 30),
                        new Potion(null, 30), new Potion(null, 30), new Potion(null, 30), new Potion(null, 30),
                        new Potion(null, -30), new Potion(null, -30)))));
    }

    @Override
    public void start(Stage stage) throws Exception {
        StackPane stackPane = new StackPane();
        stackPane.getChildren().add(gameArea);
        gameArea.fixAspectRatio((double) Constants.HEIGHT / (double) Constants.WIDTH);

        Scene scene = new Scene(stackPane, Constants.WIDTH, Constants.HEIGHT);
        stage.setScene(scene);
        stage.setTitle("Formaluck");
        stage.getIcons().add(new Image(Constants.ICONS_DIR + "player.png"));

        scene.setOnKeyPressed(actionHandler);
        scene.setOnKeyReleased(actionHandler);

        final KeyFrame oneFrame = new KeyFrame(Duration.millis(1000 / Constants.FPS), loopHandler);

        TimelineBuilder.create().cycleCount(Animation.INDEFINITE).keyFrames(oneFrame).build().play();

        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
