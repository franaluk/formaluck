package cz.cvut.fel.pjv.formaluck.inventory;

import java.util.Timer;
import java.util.TimerTask;

import cz.cvut.fel.pjv.formaluck.Constants;
import cz.cvut.fel.pjv.formaluck.component.Arrow;
import cz.cvut.fel.pjv.formaluck.component.Attack;
import cz.cvut.fel.pjv.formaluck.component.Component;
import cz.cvut.fel.pjv.formaluck.component.Position;
import cz.cvut.fel.pjv.formaluck.component.attrs.WithWeapon;
import cz.cvut.fel.pjv.formaluck.engine.Event;

/**
 * Weapon
 * 
 * Used for combat system.
 */
public class Bow extends Weapon {
    public int radius = (int) (Constants.SIZE);
    private boolean fighting = false;
    private int damage = 10;

    public Bow(Position position, int damage) {
        super(position, damage);
    }

    /**
     * @return the damage
     */
    public int getDamage() {
        return damage;
    }

    @Override
    public Event use() throws Exception {
        throw new Exception("Method Event use(Component component) must be used.");
    }

    @Override
    public Event use(WithWeapon owner) {
        this.owner = (Component) owner;

        return new Event(app -> {
            if (fighting)
                return;
                
            fighting = true;

            Position op = ((Component) owner).getPosition();
            int xCenter = (radius - op.h) / 2;
            int yCenter = (radius - op.w) / 2;

            Position attackPosition = new Position(op.x - xCenter, op.y - yCenter, radius, radius);
            Arrow arrow = new Arrow(attackPosition, owner.getWeapon(), app.player.getLastDirection());
            app.newComponents.add(arrow);

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    fighting = false;
                }
            }, owner.getAttackSpeed());
        });
    }
}