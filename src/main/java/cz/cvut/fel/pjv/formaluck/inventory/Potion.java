package cz.cvut.fel.pjv.formaluck.inventory;

import cz.cvut.fel.pjv.formaluck.Constants;
import cz.cvut.fel.pjv.formaluck.component.Component;
import cz.cvut.fel.pjv.formaluck.component.Position;
import cz.cvut.fel.pjv.formaluck.engine.Event;
import cz.cvut.fel.pjv.formaluck.inventory.InventoryItem;
import javafx.scene.image.Image;

/**
 * Potion
 * 
 * Can increase or decrease health, stamina or give new Weapon.
 */
public class Potion extends InventoryItem {
    int strong;

    public Potion(Position position, int strong) {
        super(position);
        this.strong = strong;

        if (strong < 0)
            image = new Image(Constants.ICONS_DIR + "poison-potion.png");
    }

    @Override
    public Event use() {
        return new Event(app -> {
            app.player.changeInventoryItem().trigger(app);
            app.player.setHealth(app.player.getHealth() + strong);
            app.inventory.items.remove(this);
        });
    }

    @Override
    public String getImage() {
        return "health-potion";
    }
}