package cz.cvut.fel.pjv.formaluck.gui;

import cz.cvut.fel.pjv.formaluck.component.Position;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Bar
 * 
 * A bar for drawing UI, eg. healthbar
 */
public class Bar {
    Position position;
    double percentage;
    Color color;
    Color bacColor;

    public Bar(Position position, double percentage, Color color, Color bacColor) {
        this.position = position;
        this.percentage = percentage;
        this.color = color;
        this.bacColor = bacColor;
    }

    public void render(GraphicsContext gc) {
        gc.setFill(bacColor);
        gc.fillRect(position.x, position.y, position.w, position.h);
        gc.setFill(color);
        gc.fillRect(position.x, position.y, position.w * percentage, position.h);
    }
}