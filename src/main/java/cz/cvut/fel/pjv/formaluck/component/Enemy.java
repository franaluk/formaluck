package cz.cvut.fel.pjv.formaluck.component;

import cz.cvut.fel.pjv.formaluck.component.attrs.WithHealth;
import cz.cvut.fel.pjv.formaluck.component.attrs.WithWeapon;
import cz.cvut.fel.pjv.formaluck.engine.Event;
import cz.cvut.fel.pjv.formaluck.inventory.Weapon;

/**
 * Enemy
 */
public class Enemy extends Component implements WithHealth, WithWeapon {
    Weapon Weapon;
    int health = 100;

    public Enemy(Position position) {
        super(position);

        this.Weapon = new Weapon(null, 10);
    }

    @Override
    public Event tick() {
        if (health <= 0) {
            return new Event(app -> {
                app.components.remove(this);
            });
        }

        return new Event(app -> {
            Position playerPos = app.player.getPosition();
            int x = position.x - playerPos.x, y = position.y - playerPos.y;
            int absX = Math.abs(x), absY = Math.abs(y);

            if (absX < Weapon.radius && absY < Weapon.radius) {
                Weapon.use(this).trigger(app);
            }
            
            if (absX > absY) {
                if (x < 0)
                    position.moveRight(1);
                else
                    position.moveLeft(1);
            } else {
                if (y < 0)
                    position.moveDown(1);
                else
                    position.moveUp(1);
            }
        });
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public void setHealth(int health) {
        this.health = health;
    }

    @Override
    public Weapon getWeapon() {
        return Weapon;
    }

    @Override
    public int getAttackSpeed() {
        return 1000;
    }

    @Override
    public int getAttackTime() {
        return 200;
    }

    @Override
    public void setWeapon(Weapon Weapon) {
        //
    }
}