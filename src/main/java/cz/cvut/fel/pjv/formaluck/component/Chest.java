package cz.cvut.fel.pjv.formaluck.component;

import java.util.ArrayList;

import cz.cvut.fel.pjv.formaluck.engine.Event;
import cz.cvut.fel.pjv.formaluck.inventory.InventoryItem;

/**
 * Chest
 * 
 * Can increase or decrease health, stamina or give new Weapon.
 */
public class Chest extends Component {
    ArrayList<InventoryItem> items;

    public Chest(Position position) {
        this(position, new ArrayList<InventoryItem>());
    }

    public Chest(Position position, ArrayList<InventoryItem> items) {
        super(position);
        this.items = items;
    }

    @Override
    public Event interactWith(Component component) {
        if (component instanceof Attack) {
            return new Event(app -> {
                app.components.remove(this);

                for (InventoryItem item : items) {
                    item.setPosition(position);

                    app.components.add(item);
                }
            });
        }

        return null;
    }
}