package cz.cvut.fel.pjv.formaluck.component.attrs;

import cz.cvut.fel.pjv.formaluck.inventory.Weapon;

/**
 * WithWeapon
 */
public interface WithWeapon {

    public Weapon getWeapon();

    public void setWeapon(Weapon Weapon);

    public int getAttackSpeed();

    public int getAttackTime();
}