package cz.cvut.fel.pjv.formaluck.engine;

import java.util.ArrayList;
import java.util.Iterator;

import cz.cvut.fel.pjv.formaluck.App;
import cz.cvut.fel.pjv.formaluck.Constants;
import cz.cvut.fel.pjv.formaluck.component.Component;
import cz.cvut.fel.pjv.formaluck.component.Position;
import cz.cvut.fel.pjv.formaluck.gui.Bar;
import cz.cvut.fel.pjv.formaluck.inventory.InventoryItem;
import cz.cvut.fel.pjv.formaluck.inventory.Weapon;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * GameArea
 * 
 * Canvas for painting the game.
 */
public class GameArea extends Canvas {
    App app;

    public GameArea(App app) {
        this.app = app;
    }

    /**
     * Fix aspect ratio.
     */
    public void fixAspectRatio(double aspectRatio) {
        Parent parent = getParent();

        if (parent instanceof Pane) {
            widthProperty().bind(((Pane) getParent()).widthProperty());
            heightProperty().bind(((Pane) getParent()).heightProperty());
        }
    }

    /**
     * Called every cycle.
     */
    public void redraw() {
        GraphicsContext gc = getGraphicsContext2D();

        Iterator<Event> ie = app.events.iterator();

        while (ie.hasNext()) {
            Event event = null;

            event = ie.next();

            if (event != null) {
                event.trigger(app);
            }
        }

        app.events = new ArrayList<Event>();

        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, Constants.AREA_WIDTH, Constants.AREA_HEIGHT);

        for (int i = 0; i < app.components.size(); i++) {
            Component component = app.components.get(i);
            Event e = null;

            e = component.tick();

            if (e != null)
                e.trigger(app);

            for (int j = 0; j < app.components.size(); j++) {
                Component iComponent = app.components.get(j);

                if (component == iComponent)
                    continue;

                if (component.getPosition().getBounds().intersects(iComponent.getPosition().getBounds())) {
                    app.events.add(component.interactWith(iComponent));
                }
            }

            component.render(gc);
        }

        for (Component comp : app.newComponents) {
            app.components.add(comp);
        }

        app.newComponents = new ArrayList<Component>();

        renderHud();
    }

    /**
     * Render simple HUD with healthbar, inventory and so on...
     */
    private void renderHud() {
        GraphicsContext gc = getGraphicsContext2D();

        gc.setFill(Color.WHITE);
        gc.fillRect(0, Constants.AREA_HEIGHT, Constants.WIDTH, Constants.HUD_HEIGHT);

        Bar healthBar = new Bar(new Position(0, Constants.AREA_HEIGHT, Constants.WIDTH / 2, Constants.SIZE),
                (double) app.player.getHealth() / (double) app.player.getMaxHealth(), Color.RED, Color.GREY);

        healthBar.render(gc);

        for (int i = 0; i < app.inventory.items.size(); i++) {
            InventoryItem item = app.inventory.items.get(i);
            Position position = item.getPosition();

            position.x = i * Constants.SIZE;
            position.y = Constants.AREA_HEIGHT + Constants.SIZE;
            position.w = position.h = Constants.SIZE;

            item.render(gc);

            if (item == app.player.getInventoryItem()) {
                int quarter = Constants.SIZE / 2;
                gc.setFill(Color.YELLOW);
                gc.fillOval(position.x + quarter, position.y, quarter, quarter);
            }

            if (item instanceof Weapon && item == app.player.getWeapon()) {
                int quarter = Constants.SIZE / 2;
                gc.setFill(Color.BLACK);
                gc.fillOval(position.x + quarter, position.y + quarter, quarter, quarter);
            }
        }
    }
}