package cz.cvut.fel.pjv.formaluck.component;

import cz.cvut.fel.pjv.formaluck.Constants;
import cz.cvut.fel.pjv.formaluck.engine.Event;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * Component
 * 
 * Renderable component on game area.
 */
public abstract class Component {

    protected Position position;
    protected Image image;

    public Component(Position position) {
        this.position = position;
        try {
            this.image = new Image(Constants.ICONS_DIR + getImage() + ".png");
        } catch (IllegalArgumentException e) {
            this.image = new Image(Constants.ICONS_DIR + "error.png");
        }
    }

    /**
     * Called if has interaction with another component.
     */
    public Event interactWith(Component component) {
        return null;
    }

    /**
     * Called every loop. Returns event with changes.
     */
    public Event tick() {
        return null;
    }

    /**
     * Get position on game area.
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Set position on game area.
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * Render component using graphics context.
     */
    public void render(GraphicsContext gc) {
        gc.drawImage(image, position.x, position.y, position.w, position.h);
    }

    /**
     * Get base name of image without ending
     */
    public String getImage() {
        return getClass().getSimpleName().toLowerCase();
    }
}
